
/*
 * Route
 */

var crypto = require('crypto')
    ,fs = require('fs')
    ,formidable = require('formidable')
    ,User = require('../models/user')
    ,Post = require('../models/post')
    ,Comment = require('../models/comment');

module.exports = function(app) {

  app.get('/', function(req, res){
    var page = req.query.page ? parseInt(req.query.page) : 1;
    var limit = 6;
    Post.getPostsPerPage(null, page, limit, null,function(err, total, posts){
      if(err){
        posts = [];
      }
      var totalPages = Math.ceil(total/limit);
      res.render('posts', {
        title: 'Home',
        posts: posts,
        user: req.session.user,
        currentPage: page,
        totalPages: totalPages,
        success: req.flash('success').toString(),
        error: req.flash('error').toString()
      });
    });
  });

  app.get('/reg', checkNotLogin);
  app.get('/reg', function(req, res){
    res.render('reg', {
      title: 'Register',
      user: req.session.user,
      success: req.flash('success').toString(),
      error: req.flash('error').toString()
    });
  });

  app.post('/reg', checkNotLogin);
  app.post('/reg', function(req, res){
    var username = req.body.username,
        password = req.body.password,
        confirm = req.body.confirm,
        email = req.body.email;
    if(password != confirm){
      req.flash('error', 'Password can not confirm!!!');
      return res.redirect('/reg');
    }

    //生成密码的 md5 值
    var md5 = crypto.createHash('md5'),
        md5password = md5.update(password).digest('hex');
    var newUser = new User({
                    username: username,
                    password: md5password,
                    email: email
                  });
    User.get(newUser.username, function(err, user){
      if(user){
        req.flash('error', 'User has Exists!');
        return res.redirect('/reg');
      }

      newUser.save(function(err, user){
        if(err){
          req.flash('error', err);
          return res.redirect('/reg');
        }
        req.session.user = user;
        req.flash('success', 'welcome!');
        res.redirect('/');
      });
    });
  });

  app.get('/login', checkNotLogin);
  app.get('/login', function(req, res){
    res.render('login', {
      title: 'Login',
      user: req.session.user,
      success: req.flash('success').toString(),
      error: req.flash('error').toString()
    });
  });

  app.post('/login', checkNotLogin);
  app.post('/login', function(req, res){
    var path = req.query.path;
    var username = req.body.username,
        password = req.body.password;
    var md5 = crypto.createHash('md5'),
        md5password = md5.update(password).digest('hex');
    User.get(username, function(err, user){
      if( err || !user ){
        req.flash('error', "username or password is error!");
        return res.redirect('/login?path='+path);
      }
      if(user.password != md5password){
        req.flash('error', "username or password is error!");
        return res.redirect('/login?path='+path);
      }

      req.session.user = user;
      req.flash('success', 'welcome back!');
      res.redirect(path ? path : '/');
    })
  });

  app.get('/post', checkLogin);
  app.get('/post', function(req, res){
    res.render('newpost', {
      title: 'Post',
      user: req.session.user,
      success: req.flash('success').toString(),
      error: req.flash('error').toString()
    });
  });

  app.post('/post', checkLogin);
  app.post('/post', function(req, res){
    var tags = [];
    if(req.body.tag1){
      tags.push(req.body.tag1);
    }
    if(req.body.tag2){
      tags.push(req.body.tag2);
    }
    if(req.body.tag3){
      tags.push(req.body.tag3);
    }
    var currentUser = req.session.user,
        post = new Post({
          username: currentUser.username,
          title: req.body.title,
          tags: tags,
          content: req.body.content,
          pv: 0
        });
        post.save(function(err){
          if(err){
            req.flash('error', err);
            return res.redirect('/');
          }
          req.flash('success', 'Published');
          res.redirect('/');
        });
  });

  app.get('/logout', function(req, res){
    req.session.user = null;
    req.flash('success', 'Logout successfull!');
    res.redirect('/');
  });

  app.get('/upload', checkLogin);
  app.get('/upload', function(req, res){
    res.render('upload', {
      title: 'Upload',
      user: req.session.user,
      success: req.flash('success').toString(),
      error: req.flash('error').toString()
    });
  });

  app.post('/upload', checkLogin);
  app.post('/upload', function(req, res){
    var form = new formidable.IncomingForm({keepExtensions: true, uploadDir: './public/images'});
    form.parse(req, function(err, fields, files){
      if(err){
        req.flash("error",err);
        res.redirect('/upload');
      }
      for(var i in files){
        if(files[i].size == 0){
          // delete empty file.
          fs.unlinkSync(files[i].path);
          console.log('Remove empty file successfull!');
        }else{
          var target_path = './public/images/' + files[i].name;
          // rename the file
          fs.renameSync(files[i].path, target_path);
        }
      }
      req.flash("success", "Upload Successfull!");
      res.redirect('/upload');
    });
  });

  app.get('/u/:username', function(req, res){
    User.get(req.params.username, function(err, user){
      if(!user){
        req.flash('error', 'User not Exists!');
        return res.redirect('/');
      }
      var page = req.query.page ? parseInt(req.query.page) : 1;
      var limit = 6;
      Post.getPostsPerPage(user.username, page, limit, null,function(err, total, posts){
        if(err){
          posts = [];
        }
        var totalPages = Math.ceil(total/limit);
        res.render('posts', {
          title: user.username,
          posts: posts,
          user: req.session.user,
          currentPage: page,
          totalPages: totalPages,
          success: req.flash('success').toString(),
          error: req.flash('error').toString()
        });
      });
    });
  });

  app.get('/posts/:_id', function(req, res){
    var currentUsername = null;
    if(req.session.user){
      currentUsername = req.session.user.username;
    }
    Post.getPost(req.params._id, currentUsername, function(err, post){
      if(err){
        req.flash('error', err);
        return res.redirect('/');
      }
      var title = post.title;
      if(!title){
        title = "Tweet";
      }
      res.render('article', {
        title: title,
        post: post,
        user: req.session.user,
        success: req.flash('success').toString(),
        error: req.flash('error').toString()
      });
    });
  });

  app.post('/posts/:_id', checkLogin);
  app.post('/posts/:_id', function(req,res){
    var date = new Date(),
      UTCDate = new Date(
        date.getUTCFullYear(),
        date.getUTCMonth(),
        date.getUTCDate(),
        date.getUTCHours(),
        date.getUTCMinutes(),
        date.getUTCSeconds()
      ),
      localDate = new Date(UTCDate.getTime() + 8* 60 * 60 *1000),
      monthOfYear = localDate.getMonth() < 9 ? '0' + (localDate.getMonth()+1) : (localDate.getMonth()+1),
      dayOfMonth = localDate.getDate() < 10 ? '0' + localDate.getDate() : localDate.getDate();
    var time = {
      date: localDate,
      year: localDate.getFullYear(),
      month: localDate.getFullYear() + "-" + monthOfYear,
      day: localDate.getFullYear() + "-" + monthOfYear + "-" + dayOfMonth,
      minute: localDate.getFullYear() + "-" + monthOfYear + "-" + dayOfMonth + " " +
      (localDate.getHours() < 10 ? '0' + localDate.getHours() : localDate.getHours()) + ":" + (localDate.getMinutes() < 10 ? '0' + localDate.getMinutes() : localDate.getMinutes())
    };
    var comment = {
      username: req.session.user.username,
      content: req.body.content,
      time: time
    };
    var newComment = new Comment(req.params._id, comment);
    newComment.save(function(err){
      if(err){
        req.flash('error', err);
        return res.redirect('back');
      }
      req.flash('success', "Comment successfull!");
      return res.redirect('back');
    });
  });

  app.get('/edit/:_id', checkLogin);
  app.get('/edit/:_id', function(req, res){
    var currentUser = req.session.user;
    Post.edit(req.params._id, function(err, post){
      if(err){
        req.flash('error', err);
        return res.redirect('back');
      }
      if(!post.title){
        req.flash('error', "Could not edit Tweet!");
        return res.redirect('/');
      }
      if(post.username == currentUser.username){
        res.render('edit', {
          title: post.title,
          post: post,
          user: req.session.user,
          success: req.flash('success').toString(),
          error: req.flash('error').toString()
        });
      }else{
        req.flash('error', "Permission Denied!");
        res.redirect('/');
      }
    });
  });

  app.post('/edit/:_id', checkLogin);
  app.post('/edit/:_id', function(req, res){
    var currentUser = req.session.user;
    Post.getPost(req.params._id, currentUser.username, function(err, post){
      var url = "/posts/" + req.params._id;
      if(err){
        req.flash('error', err);
        return res.redirect(url);
      }
      if(!post.title){
        req.flash('error', "Could not edit Tweet!");
        return res.redirect(url);
      }
      if(currentUser.username == post.username){
        Post.update(req.params._id, req.body.content, function(err){
          if(err){
            req.flash('error', err);
            return res.redirect(url);
          }
          req.flash('success', 'Updated Done!');
          res.redirect(url);
        });
      }else{
        req.flash('error', "Permission Denied!");
        res.redirect('/');
      }
    });
  })

  app.get('/remove/:_id', checkLogin);
  app.get('/remove/:_id', function(req, res){
    var currentUser = req.session.user;
    Post.getPost(req.params._id, currentUser.username, function(err, post){
      if(err){
        req.flash('error', err);
        return res.redirect('/');
      }
      if(currentUser.username == post.username){
        Post.remove(req.params._id, function(err){
          if(err){
            req.flash('error', "原文已丢失！");
            return res.redirect('back');
          }
          req.flash('success', 'Remove Successfull!');
          return res.redirect('/');
        });
      }else{
        req.flash('error', "Permission Denied!");
        res.redirect('/');
      }
    });
  });

  app.get('/reprint/:_id', checkLogin);
  app.get('/reprint/:_id', function(req, res){
    var currentUser = req.session.user,
        reprint_from = {_id: req.params._id},
        reprint_to = {username: currentUser.username};
    Post.reprint(reprint_from, reprint_to, function(err, reprint_post){
      if(err){
        req.flash('error', err);
        return res.redirect('back');
      }
      req.flash('success', '转载成功！');
      var url = '/posts/' + reprint_post._id;
      res.redirect(url);
    });
  });

  app.get('/tweets', function(req, res){
    var page = req.query.page ? parseInt(req.query.page) : 1;
    var limit = 6;
    Post.getPostsPerPage(null,page,limit,"tweets",function(err, total, tweets){
      if(err){
        tweets = [];
      }
      var totalPages = Math.ceil(total/limit);
      res.render('posts', {
        title: "Tweets",
        posts: tweets,
        user: req.session.user,
        currentPage: page,
        totalPages: totalPages,
        error: req.flash('error').toString(),
        success: req.flash('success').toString()
      });
    });
  });

  app.get('/posts', function(req, res){
    var page = req.query.page ? parseInt(req.query.page) : 1;
    var limit = 6;
    Post.getPostsPerPage(null,page,limit,"posts",function(err, total, posts){
      if(err){
        tweets = [];
      }
      var totalPages = Math.ceil(total/limit);
      res.render('posts', {
        title: "Blog",
        posts: posts,
        user: req.session.user,
        currentPage: page,
        totalPages: totalPages,
        error: req.flash('error').toString(),
        success: req.flash('success').toString()
      });
    });
  });

  app.get('/archive', function(req, res){
    Post.getArchive("posts", function(err, posts){
      if(err){
        req.flash('error', err);
        return res.redirect('/');
      }
      res.render('archive',{
        title: "Archive",
        posts: posts,
        user: req.session.user,
        error: req.flash('error').toString(),
        success: req.flash('success').toString()
      });
    });
  });

  app.get('/tags', function(req, res){
    Post.getTags(function(err, tags){
      if(err){
        req.flash('error', err);
        return res.redirect("/");
      }
      res.render('tags', {
        title: "Tags",
        tags: tags,
        user: req.session.user,
        error: req.flash("error").toString(),
        success: req.flash("success").toString()
      });
    });
  });

  app.get('/tags/:tag', function(req, res){
    Post.getPostsInTag(req.params.tag, function(err, posts){
      if(err){
        req.flash('error', err);
        return res.redirect('back');
      }
      res.render('tag',{
        title: req.params.tag,
        posts: posts,
        user: req.session.user,
        error: req.flash('error').toString(),
        success: req.flash('success').toString()
      });
    });
  });

  app.get('/search', function(req, res){
    var keywords = req.query.keywords;
    if(keywords){
      Post.search(keywords, function(err, results){
        if(err){
          req.flash('error', err);
          return res.redirect('/');
        }
        res.render('tag', {
          title: "Search Results",
          posts: results,
          user: req.session.user,
          error: req.flash('error').toString(),
          success: req.flash('success').toString()
        });
      });
    }else{
      res.redirect('/');
    }
  });

  app.use(function(req, res){
    res.render('404');
  });

  function checkNotLogin(req, res, next){
    if(req.session.user){
      req.flash('error', 'Has Login');
      res.redirect('back');
    }
    next();
  }

  function checkLogin(req, res, next){
    var path = req.path;
    if(!req.session.user){
      req.flash('error', 'Not Login!!!');
      return res.redirect('/login?path='+path);
    }
    next();
  }
};