var mongodb = require('mongodb').Db
    ,settings = require('../settings')
    ,markdown = require('markdown').markdown
    ,ObjectID = require('mongodb').ObjectID;

function Comment(postId,comment){
  this.ObjectID = new ObjectID(postId);
  this.comment = comment;
}

module.exports = Comment;

Comment.prototype.save = function(callback){
  var ObjectID = this.ObjectID,
        comment = this.comment;   
  mongodb.connect(settings.url, function(err, db){
    if(err){
      return callback(err);
    }
    db.collection('posts', function(err, collection){
      if(err){
        db.close();
        return callback(err);
      }
      collection.update(
        {
          "_id": ObjectID
        },
        {
          $push: {"comments": comment}
        },
        function(err){
          db.close();
          if(err){
            return callback(err);
          }
          callback(null);
        }
      );
    });
  });
}