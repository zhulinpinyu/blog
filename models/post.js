var mongodb = require('mongodb').Db
    ,markdown = require('markdown').markdown;
var settings = require('../settings');
var ObjectID = require('mongodb').ObjectID;

function Post(post){
  this.username = post.username;
  this.title = post.title;
  this.tags = post.tags;
  this.content = post.content;
  this.pv = post.pv;
}

module.exports = Post;

//save Post
Post.prototype.save = function(callback){
  var date = new Date(),
      UTCDate = new Date(date.getUTCFullYear(), date.getUTCMonth(), date.getUTCDate(), date.getUTCHours(), date.getUTCMinutes(), date.getUTCSeconds()),
      localDate = new Date(UTCDate.getTime() + 8* 60 * 60 *1000),
      monthOfYear = localDate.getMonth() < 9 ? '0' + (localDate.getMonth()+1) : (localDate.getMonth()+1),
      dayOfMonth = localDate.getDate() < 10 ? '0' + localDate.getDate() : localDate.getDate();
  var time = {
    date: localDate,
    year: localDate.getFullYear(),
    month: localDate.getFullYear() + "-" + monthOfYear,
    day: localDate.getFullYear() + "-" + monthOfYear + "-" + dayOfMonth,
    minute: localDate.getFullYear() + "-" + monthOfYear + "-" + dayOfMonth + " " +
    (localDate.getHours() < 10 ? '0' + localDate.getHours() : localDate.getHours()) + ":" + (localDate.getMinutes() < 10 ? '0' + localDate.getMinutes() : localDate.getMinutes())
  };
  var post = {
    username: this.username,
    time: time,
    title: this.title,
    tags: this.tags,
    content: this.content,
    pv: this.pv,
    comments: [],
    reprint_info: {}
  };

  mongodb.connect(settings.url, function(err, db){
    if(err){
      return callback(err);
    }

    //read posts collection
    db.collection('posts', function(err, collection){
      if(err){
        db.close();
        return callback(err);
      }

      //insert post in posts
      collection.insert(
        post,
        {safe: true},
        function(err){
          db.close();
          if(err){
            return callback(err);
          }
          callback(null);
        }
      );
    });
  });
};

Post.getPostsPerPage = function(username, page, limit, flag, callback){
  mongodb.connect(settings.url, function(err, db){
    if(err){
      return callback(err);
    }
    db.collection('posts', function(err, collection){
      if(err){
        db.close();
        return callback(err);
      }
      var query = {};
      if(username){
        query.username = username;
      }

      if(flag=="tweets"){
        query.title = "";
      }

      if(flag=="posts"){
        query.title = /./;
      }
      collection.count(query, function(err, total){
        collection.find(query,{
          skip: (page-1) * limit,
          limit: limit
        }).sort({
          time: -1
        }).toArray(function(err, posts){
          db.close();
          if(err){
            return callback(err);
          }
          posts.forEach(function(post){
            post.content = markdown.toHTML(post.content);
          });
          callback(err,total,posts);
        });
      });
    });
  });
};

Post.getPost = function(_id, username, callback){
  mongodb.connect(settings.url, function(err, db){
    if(err){
      return callback(err);
    }

    db.collection('posts', function(err, collection){
      if(err){
        db.close();
        return callback(err);
      }

      //according to params select record;
      collection.findOne({
        "_id": new ObjectID(_id)
      },function(err, post){
        if(err){
          db.close();
          return callback(err);
        }
        if(post){
          if(post.username != username){
            collection.update({
              "_id": new ObjectID(_id)
            },{
              $inc: {"pv": 1}
            }, function(err){
              db.close();
              if(err){
                return callback(err);
              }
            });
          }
          post.content = markdown.toHTML(post.content);
          post.comments.forEach(function(comment){
            comment.content = markdown.toHTML(comment.content);
          });
        }
        callback(null, post);
      });
    });
  });
};

Post.getArchive = function(flag, callback){
  mongodb.connect(settings.url, function(err, db){
    if (err) {
      return callback(err);
    }
    var query = {};
    if(flag == "posts"){
      query.title = /./;
    }
    db.collection('posts', function(err, collection){
      if (err) {
        db.close();
        return callback(err);
      }
      collection.find(query,{
        username: 1,
        title: 1,
        time: 1
      }).sort({
        time: -1
      }).toArray(function(err, posts){
        db.close();
        if(err){
          return callback(err);
        }
        callback(null, posts);
      });
    });
  });
};

Post.getTags = function(callback){
  mongodb.connect(settings.url, function(err, db){
    if(err){
      return callback(err);
    }

    db.collection("posts", function(err, collection){
      if(err){
        db.close();
        return callback(err);
      }

      //reutrn posts中tags键的所有不同值
      collection.distinct('tags', function(err, tags){
        db.close();
        if(err){
          return callback(err);
        }
        callback(null, tags);
      });
    });
  });
}

Post.getPostsInTag = function(tag, callback){
  mongodb.connect(settings.url, function(err, db){
    if(err){
      return callback(err);
    }
    db.collection("posts", function(err, collection){
      if(err){
        db.close();
        return callback(err);
      }
      collection.find({
        tags: tag
      },{
        username: 1,
        time: 1,
        title: 1
      }).sort({
        time: -1
      }).toArray(function(err, posts){
        db.close();
        if(err){
          return callback(err);
        }
        callback(null, posts);
      });
    })
  });
}

Post.search = function(keywords, callback){
  mongodb.connect(settings.url, function(err, db){
    if(err){
      return callback(err);
    }

    db.collection('posts', function(err, collection){
      if(err){
        db.close();
        return callback(err);
      }
      var pattern = new RegExp("^.*"+ keywords + ".*$", "i");
      collection.find({
        "title": pattern
      },{
        "username": 1,
        "time": 1,
        "title": 1
      }).sort({
        time: -1
      }).toArray(function(err, results){
        db.close();
        if(err){
          return callback(err);
        }
        callback(null, results);
      });
    });
  });
}

Post.reprint = function(reprint_from, reprint_to, callback){
  mongodb.connect(settings.url, function(err, db){
    if(err){
      return callback(err);
    }
    db.collection("posts", function(err, collection){
      if(err){
        db.close();
        return callback(err);
      }
      collection.findOne({
        "_id": new ObjectID(reprint_from._id)
      }, function(err, post){
        if(err){
          db.close();
          return callback(err);
        }
        var date = new Date(),
          UTCDate = new Date(date.getUTCFullYear(), date.getUTCMonth(), date.getUTCDate(), date.getUTCHours(), date.getUTCMinutes(), date.getUTCSeconds()),
          localDate = new Date(UTCDate.getTime() + 8* 60 * 60 *1000),
          monthOfYear = localDate.getMonth() < 9 ? '0' + (localDate.getMonth()+1) : (localDate.getMonth()+1),
          dayOfMonth = localDate.getDate() < 10 ? '0' + localDate.getDate() : localDate.getDate();
        var time = {
          date: localDate,
          year: localDate.getFullYear(),
          month: localDate.getFullYear() + "-" + monthOfYear,
          day: localDate.getFullYear() + "-" + monthOfYear + "-" + dayOfMonth,
          minute: localDate.getFullYear() + "-" + monthOfYear + "-" + dayOfMonth + " " +
          (localDate.getHours() < 10 ? '0' + localDate.getHours() : localDate.getHours()) + ":" + (localDate.getMinutes() < 10 ? '0' + localDate.getMinutes() : localDate.getMinutes())
        };

        delete post._id

        post.username = reprint_to.username;
        post.time = time;
        post.title = (post.title.search(/[转载]/) > -1) ? post.title : "[转载]" + post.title;
        post.comments = [];
        post.reprint_info = {
          "reprint_from": reprint_from
        };
        post.pv = 0;

        collection.update({
          "_id": new ObjectID(reprint_from._id)
        },{
          $push: {
            "reprint_info.reprint_to": {
              "username": post.username,
              "day": time.day,
              "title": post.title
            }
          }
        }, function(err){
          if(err){
            db.close();
            return callback(err);
          }
        });

        collection.insert(post, {
          safe: true
        }, function(err, reprint_post){
          db.close();
          if(err){
            return callback(err);
          }
          callback(null, reprint_post[0]);
        });
      });
    });
  });
}

Post.edit = function(_id, callback){
  mongodb.connect(settings.url, function(err, db){
    if(err){
      return callback(err);
    }
    db.collection('posts', function(err, collection){
      if(err){
        db.close();
        return callback(err);
      }
      collection.findOne({
        "_id": new ObjectID(_id)
      }, function(err, post){
        db.close();
        if(err){
          return callback(err);
        }
        callback(null, post);
      });
    });
  });
};

Post.update = function(_id, content, callback){
  mongodb.connect(settings.url, function(err, db){
    if(err){
      return callback(err);
    }
    db.collection('posts', function(err, collection){
      if(err){
        db.close();
        return callback(err);
      }
      collection.update({
        "_id": new ObjectID(_id)
      },{
        $set: {content: content}
      }, function(err){
        db.close();
        if(err){
          return callback(err);
        }
        callback(null);
      });
    });
  });
};

Post.remove = function(_id, callback){
  mongodb.connect(settings.url, function(err, db){
    if(err){
      return callback(err);
    }
    db.collection('posts', function(err, collection){
      if(err){
        db.close();
        return callback(err);
      }
      collection.findOne({
        "_id": new ObjectID(_id)
      }, function(err, post){
        if(err){
          db.close();
          return callback(err);
        }
        var reprint_from = "";
        if(post.reprint_info.reprint_from){
          reprint_from = post.reprint_info.reprint_from;
        }
        if(reprint_from !=""){
          collection.update({
            "_id": new ObjectID(reprint_from._id)
          }, {
            $pull: {
              "reprint_info.reprint_to": {
                "username": post.username,
                "day": post.time.day,
                "title": post.title
              }
            }
          }, function(err){
            if(err){
              console.log("err");
              db.close();
              return callback(err);
            }
          });
        }
      });

      collection.remove({
        "_id": new ObjectID(_id)
      }, {
        w: 1
      },function(err){
        db.close();
        if(err){
          return callback(err);
        }
        callback(null);
      });
    });
  });
};