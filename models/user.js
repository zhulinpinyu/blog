var mongodb = require('mongodb').Db;
var settings = require('../settings');

function User(user){
  this.username = user.username;
  this.password = user.password;
  this.email = user.email;
};

module.exports = User;

//save user
User.prototype.save = function(callback) {
  var user = {
    username: this.username,
    password: this.password,
    email: this.email
  };

  //open db
  mongodb.connect(settings.url, function(err, db){
    if(err) {
      return callback(err);
    }

    // read users collection
    db.collection('users', function(err, collection){
      if(err){
        db.close();
        return callback(err);
      }
      //insert user in users
      collection.insert(
        user,
        { safe: true },
        function(err, user){
          db.close();
          if(err){
            return callback(err);
          }
          callback(null,user[0]);
        }
      );
    });
  });
};

User.get = function(username, callback){
  mongodb.connect(settings.url, function(err, db){
    if(err){
      return callback(err);
    }

    db.collection('users', function(err, collection){
      if(err){
        db.close();
        return callback(err);
      }
      collection.findOne(
        {username: username},
        function(err, user){
          db.close();
          if(err){
            return callback(err);
          }
          callback(null, user);
        }
      );
    });
  });
};